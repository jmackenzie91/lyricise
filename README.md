# lyricise

GoLyrics is a Golang command-line interface (CLI) application designed to recursively scan a provided directory,
fetch lyrics for each MP3 music file, and store them in corresponding lyric files.
This requires oauth authentication with genius.com. Once authenticated, the user's token is stored in the root 
directory, eliminating the need for re-authentication until the token expires.


## Installation

First install [Go](http://golang.org).

If you just want to install the binary to your current directory and don't care about the source code, run

## Running test suite
To run the test suite please contact me directly. The mp3 files used to test are not available on VCS due to copyright issues.

## Installation

To install lyricise, you need to have Golang installed on your system. Once Golang is installed,
run the following command:

```bash
git clone git:gitlab.com/jmackenzie91/lyricise.git
```

## Usage

1. Navigate to the directory containing your MP3 music files.
2. Run the following command to initiate the recursive lyrics fetching process:
```bash
$ lyricise --verbose --path /path/to/mp3/collection
3. The application will prompt you to authenticate with genius.com on the first run. Follow the instructions to complete the OAuth process.
4. After authentication, the application will fetch and store lyrics for each MP3 file in the corresponding lyric files.
```