package client

import (
	"fmt"
	"github.com/johnmackenzie91/lyricise/cmd/lyricise/internal/genius/auth"
	"github.com/sirupsen/logrus"
	"io"
	"net/http"
	"time"
)

var apiHost = "api.genius.com"

type Client struct {
	inner  *http.Client
	logger *logrus.Logger
	token  auth.AccessToken
}

type metadata struct {
	Status int `json:"status"`
}

func NewClient(accessToken auth.AccessToken, logger *logrus.Logger) *Client {
	return &Client{
		token: accessToken,
		inner: &http.Client{
			Timeout: time.Second * 10,
		},
		logger: logger,
	}
}

func (c *Client) get(url string, withRetries bool) ([]byte, error) {
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", c.token))

	resp, err := c.inner.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	c.logger.WithFields(logrus.Fields{
		"status": resp.StatusCode,
		"urk":    url,
	}).Debug("response received")

	if resp.StatusCode >= 400 {
		return nil, fmt.Errorf("request failed with code %d", resp.StatusCode)
	}

	return io.ReadAll(resp.Body)
}
