package client

import (
	"encoding/json"
	"net/url"
)

type searchResponse struct {
	Metadata   metadata   `json:"meta"`
	SearchData SearchData `json:"response"`
}

type SearchData struct {
	Hits []Hit `json:"hits"`
}

type SearchResultType string

type Hit struct {
	Type   SearchResultType `json:"type"`
	Result SearchResult     `json:"result"`
}

type SearchResult struct {
	ID   int    `json:"id"`
	Text string `json:"full_title"`
	Path string `json:"path"`
}

func (c *Client) SearchSongs(term string) ([]SearchResult, error) {
	searchURL := url.URL{
		Scheme: "https",
		Host:   apiHost,
		Path:   "/search",
	}
	q := searchURL.Query()
	q.Set("q", term)
	searchURL.RawQuery = q.Encode()

	data, err := c.get(searchURL.String(), true)
	if err != nil {
		return nil, err
	}

	var resp searchResponse
	if err := json.Unmarshal(data, &resp); err != nil {
		return nil, err
	}

	var results []SearchResult
	for _, result := range resp.SearchData.Hits {
		results = append(results, result.Result)
	}
	return results, nil
}
