package auth

import (
	"errors"
	"os"
	"path/filepath"
)

var ErrInvalidToken = errors.New("invalid token")

func LoadCachedToken() (AccessToken, error) {
	dirname, err := os.UserHomeDir()
	if err != nil {
		return "", err
	}
	data, err := os.ReadFile(filepath.Join(dirname, ".lyricise"))
	if err != nil {
		return "", err
	}
	token := AccessToken(data)

	//if isValidToken(token) {
	//	return token, ErrInvalidToken
	//}

	return token, nil
}

//func isValidToken(token AccessToken) bool {
//	_, err := client.NewClient(token, logrus.New()).SearchSongs("test")
//	return err == nil
//}
