package auth

import (
	"context"
	"errors"
	"github.com/sirupsen/logrus"
	"net/http"
)

func Authenticate(logger *logrus.Logger) (AccessToken, error) {
	token, err := LoadCachedToken()
	if err == nil {
		return token, nil
	}

	logger.WithFields(logrus.Fields{"err": err.Error()}).Info("unable to load token from cache")
	logger.Info("Authentication required. Check your browser.")

	return OauthStrategy(logger)
}

type AccessToken string

var ErrTimeout = errors.New("authentication flow timed out")
var ErrInvalidState = errors.New("invalid state")

type Server struct {
	state string

	errCh   chan error
	tokenCh chan AccessToken
	close   chan struct{}

	server *http.Server
	logger *logrus.Logger
}

func NewAuthServer(state string, logger *logrus.Logger) (string, Server) {
	s := Server{
		state:   state,
		errCh:   make(chan error),
		tokenCh: make(chan AccessToken),
		close:   make(chan struct{}),
		logger:  logger,
	}

	r := http.NewServeMux()
	r.HandleFunc("/", s.rootHandler)
	r.HandleFunc("/receive", s.receiveHandler)

	s.server = &http.Server{
		Handler: r,
		Addr:    ":62626",
	}

	return "http://localhost:62626", s
}

// Serve hangs until the server is closed.
func (s *Server) Serve() error {
	if err := s.server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		return err
	}
	return nil
}

func (s Server) receiveHandler(w http.ResponseWriter, r *http.Request) {
	accessToken := r.URL.Query().Get("access_token")
	actualState := r.URL.Query().Get("state")
	if actualState != s.state {
		s.logger.Warn("Authentication failed due to invalid state - please try again")
		w.Write([]byte("Authentication error - please go back to your terminal and try again."))
		s.errCh <- ErrInvalidState
		return
	}
	s.tokenCh <- AccessToken(accessToken)
	w.Write([]byte("You can now close this tab and check your terminal."))
	s.logger.Debug("token successfully received")
}

func (s Server) rootHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(`<script>document.location=document.location.href.replace("#","receive?");</script>`))
}

func (s Server) WaitForAuth(ctx context.Context) (AccessToken, error) {
	defer s.Shutdown(ctx) //nolint: errcheck I can not think of a reason to handle this error

	select {
	case <-ctx.Done():
		return "", ErrTimeout
	case err := <-s.errCh:
		return "", err
	case token := <-s.tokenCh:
		_ = saveTokenToCache(token)
		return token, nil
	}
}

func (s *Server) Shutdown(ctx context.Context) error {
	err := s.server.Shutdown(ctx)
	if err != nil {
		return err
	}
	return err
}
