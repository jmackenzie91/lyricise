package auth

import (
	"context"
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"time"
)

const (
	apiHost  = "api.genius.com"
	clientID = "YUzgT8ekyc89yyG3JDYGInAvmQ2vBg_b54QhbyQGlgL_iZa0fB2CuodWxhcYkLrH"
	scope    = "me"
)

func OauthStrategy(logger *logrus.Logger) (AccessToken, error) {
	stateBytes := make([]byte, 32)
	if _, err := rand.Reader.Read(stateBytes); err != nil {
		return "", err
	}
	state := hex.EncodeToString(stateBytes)

	addr, svr := NewAuthServer(state, logger)

	go func() {
		svr.Serve()
	}()

	url := fmt.Sprintf(
		"https://%s/oauth/authorize?client_id=%s&redirect_uri=%s&scope=%s&state=%s&response_type=token",
		apiHost,
		clientID,
		addr,
		scope,
		state,
	)

	if err := exec.Command("open", url).Run(); err != nil {
		return "", err
	}

	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	token, err := svr.WaitForAuth(ctx)
	if err != nil {
		logger.WithFields(logrus.Fields{"msg": "failed to get authenticate"}).Error(err)

		return token, err
	}

	logger.Info("Authentication successful.")

	return token, saveTokenToCache(token)
}

func saveTokenToCache(token AccessToken) error {
	dirname, err := os.UserHomeDir()
	if err != nil {
		return err
	}
	return ioutil.WriteFile(filepath.Join(dirname, ".lyric"), []byte(token), 0600)
}
