package auth

import (
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"net/http"
	"testing"
	"time"
)

func TestServer_Run_times_out(t *testing.T) {
	_, sut := NewAuthServer("aaa", logrus.New())

	done := make(chan struct{})

	// will timeout before the test
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	go func() {
		token, err := sut.WaitForAuth(ctx)
		assert.Empty(t, token)
		assert.ErrorIs(t, ErrTimeout, err)
		done <- struct{}{}
	}()

	select {
	case <-time.After(5 * time.Second):
		t.Fatal("timed out waiting for test")
	case <-done:
	}
}

func TestServer_Run_token_supplied_by_callback_is_returned(t *testing.T) {
	addr, sut := NewAuthServer("aaa", logrus.New())

	done := make(chan struct{})

	go func() {
		err := sut.Serve()
		assert.NoError(t, err)
	}()

	defer func() {
		err := sut.Shutdown(context.Background())
		assert.NoError(t, err)
	}()

	go func() {
		token, err := sut.WaitForAuth(context.Background())
		assert.Equal(t, AccessToken("test-token"), token)
		assert.NoError(t, err)
		done <- struct{}{}
	}()

	res, err := http.Get(fmt.Sprintf("%s/receive?access_token=test-token&state=aaa", addr))
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, res.StatusCode)

	select {
	case <-time.After(8 * time.Second):
		t.Fatal("timed out waiting for test")
	case <-done:
	}
}

func TestServer_Run_token_not_supplied_when_state_does_not_match(t *testing.T) {
	addr, sut := NewAuthServer("aaa", logrus.New())

	done := make(chan struct{})

	go func() {
		err := sut.Serve()
		require.NoError(t, err)
	}()

	defer func() {
		err := sut.Shutdown(context.Background())
		assert.NoError(t, err)
	}()

	go func() {
		token, _ := sut.WaitForAuth(context.Background()) //nolint: errcheck I am not testing the error value
		assert.Empty(t, token)
		done <- struct{}{}
	}()

	_, err := http.Get(fmt.Sprintf("%s/receive?access_token=test-token&state=bbb", addr))
	assert.NoError(t, err)

	select {
	case <-time.After(5 * time.Second):
		t.Fatal("timed out waiting for test")
	case err := <-sut.errCh:
		require.ErrorIs(t, err, ErrInvalidState)
	case <-done:
	}
}
