package throttle

import (
	"github.com/johnmackenzie91/lyricise/cmd/lyricise/internal/genius/client"
	"go.uber.org/ratelimit"
)

type ThrottledClient struct {
	client  client.Client
	limiter ratelimit.Limiter
}

func NewClient(client client.Client) *ThrottledClient {
	rl := ratelimit.New(1) // per second

	return &ThrottledClient{
		client:  client,
		limiter: rl,
	}
}

func (t *ThrottledClient) SearchSongs(term string) ([]client.SearchResult, error) {
	t.limiter.Take()
	return t.client.SearchSongs(term)
}

func (t *ThrottledClient) GetLyrics(uri string) (client.Lyrics, error) {
	t.limiter.Take()
	return t.client.GetLyrics(uri)
}

func (t *ThrottledClient) GetSong(id int) (*client.Song, error) {
	t.limiter.Take()
	return t.client.GetSong(id)
}
