package library

import (
	"fmt"
	"os"
	"strings"
)

func HasCorrespondingLyricsFile(s Song) bool {
	fileName := fmt.Sprintf("%s.lrc", strings.Replace(s.Name(), ".mp3", "", 1))
	_, err := os.Stat(fileName)

	// Check if the file exists or not.
	if err != nil {
		return false
	}
	return true
}

func WriteCorrespondingLyricFile(s Song, lyrics []byte) error {
	fileName := strings.ReplaceAll(s.Name(), ".mp3", "")

	return os.WriteFile(fmt.Sprintf("%s.lrc", fileName), lyrics, 0644)
}
