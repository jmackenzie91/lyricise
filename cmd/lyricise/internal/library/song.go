package library

import (
	"github.com/dhowden/tag"
	"os"
	"path/filepath"
)

type Song struct {
	*os.File
	meta *meta
}

func NewSong(file *os.File) Song {
	return Song{file, nil}
}

type meta struct {
	artist string
	title  string
}

func (s Song) IsMP3() bool {
	return filepath.Ext(s.Name()) == ".mp3"
}

func (s *Song) GetArtistAndTitle() (string, string, error) {
	if s.meta != nil {
		return s.meta.artist, s.meta.title, nil
	}
	s.meta = &meta{}
	tags, err := tag.ReadFrom(s)
	if err != nil {
		return "", "", err
	}

	s.meta.title = tags.Title()
	if err != nil {
		return "", "", err
	}

	s.meta.artist = tags.Artist()
	if err != nil {
		return "", "", err
	}
	return s.meta.artist, s.meta.title, nil
}
