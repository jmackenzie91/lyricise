package library

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"os"
	"testing"
)

func TestSong_IsMP3_returns_true(t *testing.T) {
	f, err := os.Create("test.mp3")
	assert.NoError(t, err)

	defer func() {
		err := os.RemoveAll(f.Name())
		assert.NoError(t, err)
	}()

	sut := NewSong(f)

	assert.True(t, sut.IsMP3())
}

func TestSong_IsMP3_returns_false(t *testing.T) {
	f, err := os.Create("test.docx")
	assert.NoError(t, err)

	defer func() {
		err := os.RemoveAll(f.Name())
		assert.NoError(t, err)
	}()

	sut := NewSong(f)

	assert.False(t, sut.IsMP3())

}

func TestSong_GetArtistAndTitle(t *testing.T) {
	f, err := os.Open("./testdata/01 - Arctic Monkeys - Do I Wanna Know.mp3")
	require.NoError(t, err)

	s := NewSong(f)
	artist, title, err := s.GetArtistAndTitle()
	require.NoError(t, err)
	assert.Equal(t, "Arctic Monkeys", artist)
	assert.Equal(t, "Do I Wanna Know?", title)
}

func TestSong_GetArtistAndTitle_fetch_from_meta_the_second_time_around(t *testing.T) {
	f, err := os.Open("./testdata/01 - Arctic Monkeys - Do I Wanna Know.mp3")
	require.NoError(t, err)

	s := NewSong(f)
	_, _, err = s.GetArtistAndTitle()
	require.NoError(t, err)

	// setting the file to nil will fail if the artist and title is not fetched from meta
	// thus ensuring our expected code flow
	s.File = nil

	artist, title, err := s.GetArtistAndTitle()
	require.NoError(t, err)
	assert.Equal(t, "Arctic Monkeys", artist)
	assert.Equal(t, "Do I Wanna Know?", title)
}
