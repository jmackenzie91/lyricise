package library

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"os"
	"testing"
)

func TestHasCorrespondingLyricsFile(t *testing.T) {
	inputF, err := os.Open("./testdata/some-song.mp3")
	assert.NoError(t, err)

	input := NewSong(inputF)
	got := HasCorrespondingLyricsFile(input)
	assert.True(t, got)
}

func TestWriteCorrespondingLyricFile(t *testing.T) {
	inputSong := Song{
		File: os.NewFile(uintptr(3), "./testdata/test-song.mp3"),
	}
	inputLyrics := []byte("some lyrics")
	err := WriteCorrespondingLyricFile(inputSong, inputLyrics)
	require.NoError(t, err)

	_, err = os.Stat("./testdata/test-song.lrc")
	assert.NoError(t, err)

	assert.NoError(t, os.Remove("./testdata/test-song.lrc"))
}
