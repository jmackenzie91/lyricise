package app

import (
	"bytes"
	"fmt"
	"github.com/johnmackenzie91/lyricise/cmd/lyricise/internal/genius/client"
)

func resolveLyricsGenius(geniusClient Client, artist, title string) ([]byte, error) {
	songs, err := geniusClient.SearchSongs(fmt.Sprintf("%s - %s", artist, title))
	if err != nil {
		return nil, fmt.Errorf("Failed to search: %w", err)
	}

	if len(songs) == 0 {
		return nil, nil
	}

	song, err := geniusClient.GetSong(songs[0].ID)
	if err != nil {
		return nil, fmt.Errorf("Failed to retrieve lyrics: %w", err)
	}

	l, err := geniusClient.GetLyrics(song.URL)
	return formatLyrics(song.Artist.Title, song.Title, l), nil
}

func formatLyrics(artist, title string, lyrics client.Lyrics) []byte {
	output := bytes.NewBuffer([]byte{})
	output.WriteString(fmt.Sprintf("%s\n%s\n\n", title, artist))

	for _, verse := range lyrics.Verses {
		if verse.Label != "" {
			output.WriteString(fmt.Sprintf("[%s]\n", verse.Label))
		}

		for _, line := range verse.Lines {
			output.WriteString(fmt.Sprintf("%s\n", line))
		}

		output.WriteString("\n")
	}
	return output.Bytes()
}
