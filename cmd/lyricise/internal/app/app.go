package app

import (
	"flag"
	"fmt"
	"github.com/johnmackenzie91/lyricise/cmd/lyricise/internal/genius/auth"
	"github.com/johnmackenzie91/lyricise/cmd/lyricise/internal/genius/client"
	"github.com/johnmackenzie91/lyricise/cmd/lyricise/internal/genius/throttle"
	"github.com/johnmackenzie91/lyricise/cmd/lyricise/internal/library"
	"github.com/sirupsen/logrus"
	"io/fs"
	"os"
	"path/filepath"
	"sync"
)

type App struct {
	logger       *logrus.Logger
	geniusClient Client
	Path         string
}

//go:generate $GOBIN/mockgen -source=app.go -destination=app_mocks_test.go -package=app
type Client interface {
	SearchSongs(term string) ([]client.SearchResult, error)
	GetLyrics(uri string) (client.Lyrics, error)
	GetSong(id int) (*client.Song, error)
}

func New() App {
	return App{
		logger: logrus.New(),
	}
}

func (app *App) ParseArgs() error {
	path := flag.String("path", "", "path to music library")
	// add check for verbose flag

	flag.BoolFunc("verbose", "show verbose output", func(s string) error {
		app.logger.SetLevel(logrus.DebugLevel)
		return nil
	})

	flag.Parse()

	if path != nil && *path != "" {
		app.Path = *path
	} else {
		var err error
		app.Path, err = os.Getwd()
		if err != nil {
			return fmt.Errorf("failed to get current working directory: %w", err)
		}
	}

	return nil
}

func (app *App) Exec() (err error) {
	token, err := auth.Authenticate(app.logger)
	if err != nil {
		return fmt.Errorf("Authentication failed: %w", err)
	}

	app.geniusClient = client.NewClient(token, app.logger)
	app.geniusClient = throttle.NewClient(app.geniusClient)

	return app.walk()
}

func (a App) walk() error {
	fileCh := make(chan *os.File)
	wg := sync.WaitGroup{}

	concurrency := 5
	for i := 0; i < concurrency; i++ {
		wg.Add(1)
		go func() {
			for err := range a.processFiles(fileCh) {
				if err != nil {
					a.logger.Error(err)
				}
			}
			wg.Done()
		}()
	}

	a.logger.Debug("walking file system. starting at: ", a.Path)
	err := filepath.WalkDir(a.Path, func(path string, entry fs.DirEntry, err error) error {
		if err != nil {
			a.logger.Error(err)
			return nil
		}
		if entry == nil || entry.IsDir() {
			return nil
		}

		f, err := os.Open(path)
		if err != nil {
			a.logger.Error(err)
			return nil
		}

		fileCh <- f

		return nil
	})

	close(fileCh)
	wg.Wait()

	return err
}

func (app App) processFiles(input chan *os.File) chan error {
	output := make(chan error)

	go func() {
		for f := range input {
			app.processFile(f, output)
			if err := f.Close(); err != nil {
				app.logger.WithFields(logrus.Fields{
					"msg":  "failed to close file",
					"file": f.Name(),
				}).Error(err)
			}
		}
		close(output)
	}()

	return output
}

func (app App) processFile(f *os.File, output chan error) {
	s := library.NewSong(f)
	if !s.IsMP3() || library.HasCorrespondingLyricsFile(s) {
		return
	}

	artist, title, err := s.GetArtistAndTitle()
	if err != nil {
		output <- fmt.Errorf("failed to get artist and title: %w", err)
		return
	}

	lyrics, err := resolveLyricsGenius(app.geniusClient, artist, title)
	if err != nil {
		output <- fmt.Errorf("failed to resolve lyrics: %w", err)
		return
	}

	if err := library.WriteCorrespondingLyricFile(s, lyrics); err != nil {
		output <- fmt.Errorf("failed to write lyrics: %w", err)
		return
	}
	return
}
