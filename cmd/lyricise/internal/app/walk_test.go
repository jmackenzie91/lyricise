package app

import (
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func Test_combineErrChannels_output_channel_closes_when_input_channels_are_closed(t *testing.T) {
	errChOne := make(chan error, 1)
	errChTwo := make(chan error, 1)
	errChThree := make(chan error, 1)

	sutCh := make(chan error)
	loopChannelsComplete := make(chan struct{})
	go func() {
		loopChannels(context.Background(), sutCh, errChOne, errChTwo, errChThree)
		loopChannelsComplete <- struct{}{}
	}()

	gotErrs := make(map[string]struct{})
	done := make(chan struct{})
	go func() {
		select {
		case err := <-sutCh:
			gotErrs[err.Error()] = struct{}{}
		case <-loopChannelsComplete:
			done <- struct{}{}
		}
		done <- struct{}{}
	}()

	errChOne <- errors.New("error 1")
	errChTwo <- errors.New("error 2")
	errChThree <- errors.New("error 3")
	close(errChOne)
	close(errChTwo)
	close(errChThree)

	select {
	case <-done:
	case <-time.After(3 * time.Second):
		t.Fatal("timed out waiting for test to complete")
	}

	assert.NotNil(t, gotErrs["error 1"])
	assert.NotNil(t, gotErrs["error 2"])
	assert.NotNil(t, gotErrs["error 3"])
}

func Test_combineErrChannels_returns_when_input_channel_is_closed(t *testing.T) {
	errChOne := make(chan error, 1)
	errChTwo := make(chan error, 1)

	sutCh := make(chan error)
	loopChannelsComplete := make(chan struct{})
	go func() {
		loopChannels(context.Background(), sutCh, errChOne, errChTwo)
		loopChannelsComplete <- struct{}{}
	}()

	close(errChOne)
	close(errChTwo)

	select {
	case <-loopChannelsComplete:
	case <-time.After(3 * time.Second):
		t.Fatal("timed out waiting for test to complete")
	}

	close(sutCh)
}

func Test_combineErrChannels_returns_when_context_cancelled(t *testing.T) {
	errChOne := make(chan error, 1)
	errChTwo := make(chan error, 1)

	sutCh := make(chan error)
	loopChannelsComplete := make(chan struct{})

	ctx, cancel := context.WithCancel(context.Background())
	go func() {
		loopChannels(ctx, sutCh, errChOne, errChTwo)
		loopChannelsComplete <- struct{}{}
	}()

	cancel()

	select {
	case <-loopChannelsComplete:
	case <-time.After(3 * time.Second):
		t.Fatal("timed out waiting for test to complete")
	}

	close(errChOne)
	close(errChTwo)
	close(sutCh)
}
