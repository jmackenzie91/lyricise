package app

import (
	"context"
	"sync"
)

func CombineErrChannels(ctx context.Context, errChans ...chan error) chan error {
	out := make(chan error)
	go func() {
		loopChannels(ctx, out, errChans...)
		close(out)
	}()

	return out
}

// loopChannels hangs until context is cancelled or all channels are closed
func loopChannels(ctx context.Context, out chan error, inputChs ...chan error) {
	wg := sync.WaitGroup{}
	for _, errChan := range inputChs {
		wg.Add(1)
		go func(ch chan error) {
			for {
				err, ok := <-ch
				if !ok {
					break
				}
				out <- err
			}
			wg.Done()
		}(errChan)
	}

	waitGroupDone := make(chan struct{})
	go func() {
		wg.Wait()
		waitGroupDone <- struct{}{}
	}()

	select {
	case <-ctx.Done():
	case <-waitGroupDone:
	}

	return
}
