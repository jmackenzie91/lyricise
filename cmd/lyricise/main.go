package main

import (
	"fmt"
	"github.com/johnmackenzie91/lyricise/cmd/lyricise/internal/app"
	"os"
)

func main() {
	application := app.New()
	if err := application.ParseArgs(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	if err := application.Exec(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	os.Exit(0)
}
