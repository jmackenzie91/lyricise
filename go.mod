module github.com/johnmackenzie91/lyricise

go 1.21.1

require (
	github.com/dhowden/tag v0.0.0-20230630033851-978a0926ee25
	github.com/sirupsen/logrus v1.9.3
	github.com/stretchr/testify v1.8.4
	go.uber.org/mock v0.3.0
	go.uber.org/ratelimit v0.3.0
)

require (
	github.com/benbjohnson/clock v1.3.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/sys v0.1.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
